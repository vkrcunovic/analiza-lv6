﻿/*
 * Created by SharpDevelop.
 * User: Krcko
 * Date: 31.1.2019.
 * Time: 0:15
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Hangman
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Label labelDash;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBoxGuess;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label noLives;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.labelDash = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxGuess = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.noLives = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// labelDash
			// 
			this.labelDash.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.labelDash.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.labelDash.Location = new System.Drawing.Point(12, 9);
			this.labelDash.Name = "labelDash";
			this.labelDash.Size = new System.Drawing.Size(334, 25);
			this.labelDash.TabIndex = 0;
			this.labelDash.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.SystemColors.AppWorkspace;
			this.label1.Location = new System.Drawing.Point(12, 47);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(68, 25);
			this.label1.TabIndex = 3;
			this.label1.Text = "Enter letter:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// textBoxGuess
			// 
			this.textBoxGuess.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.textBoxGuess.Location = new System.Drawing.Point(86, 47);
			this.textBoxGuess.MaxLength = 1;
			this.textBoxGuess.Name = "textBoxGuess";
			this.textBoxGuess.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.textBoxGuess.Size = new System.Drawing.Size(24, 26);
			this.textBoxGuess.TabIndex = 4;
			this.textBoxGuess.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.SystemColors.Control;
			this.button1.Location = new System.Drawing.Point(117, 47);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(229, 59);
			this.button1.TabIndex = 5;
			this.button1.Text = "Guess!";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.SystemColors.AppWorkspace;
			this.label3.Location = new System.Drawing.Point(12, 81);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(68, 25);
			this.label3.TabIndex = 6;
			this.label3.Text = "Lives left:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// noLives
			// 
			this.noLives.BackColor = System.Drawing.SystemColors.AppWorkspace;
			this.noLives.Location = new System.Drawing.Point(86, 81);
			this.noLives.Name = "noLives";
			this.noLives.Size = new System.Drawing.Size(24, 25);
			this.noLives.TabIndex = 7;
			this.noLives.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(358, 116);
			this.Controls.Add(this.noLives);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.textBoxGuess);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.labelDash);
			this.Name = "MainForm";
			this.Text = "Hangman";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
