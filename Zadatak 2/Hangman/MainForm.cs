﻿/*
 * Created by SharpDevelop.
 * User: Krcko
 * Date: 31.1.2019.
 * Time: 0:15
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Hangman
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		List<string> wordslist= new List<string>();
		string target;
		char guess;
		int livesleft=5;
		bool right =false;
		public MainForm()
		{
			InitializeComponent();
			using (System.IO.StreamReader reader = new System.IO.StreamReader("wordslist.txt"))
			{
				target = reader.ReadLine();
				string[] parts = target.Split(',');
				foreach (string s in parts)
					wordslist.Add(s);
			}
			startGame();
			
		}
		void startGame(){
			Random rnd = new Random();
			int n = rnd.Next(0, wordslist.Count - 1);
			target = wordslist[n];
			dashes(target);}
		void Button1Click(object sender, EventArgs e)
		{	
			if(!(textBoxGuess.Text=="")){
			noLives.Text=livesleft.ToString();
			guess=textBoxGuess.Text[0];
			for (int i = 0; i < target.Length; i++) {
				if(Char.ToUpper(target[i])==Char.ToUpper(guess)){
					right = true;
					labelDash.Text=labelDash.Text.Remove(i,1);
					labelDash.Text=labelDash.Text.Insert(i,Char.ToUpper(guess).ToString());
					textBoxGuess.Text="";
					textBoxGuess.Focus();
				}
			}
			if(labelDash.Text==target){
				youWon();
				resetGame();
				startGame();
			}
			if(!right){
				livesleft--;
				noLives.Text=livesleft.ToString();
				textBoxGuess.Text="";
				textBoxGuess.Focus();
				if(livesleft==0){
					gameOver();
					resetGame();
					startGame();
				}
			}
			right=false;
			}
			else {MessageBox.Show("Unesi slovo!");
				textBoxGuess.Focus();
			}
		}
			public void dashes(string target){
				labelDash.Text="";
				for (int i = 0; i < target.Length; i++) {
					labelDash.Text=labelDash.Text.Insert(i,"X");
				}
				
			}
		private void youWon(){
			MessageBox.Show("You won!");
		}
		private void gameOver(){
			MessageBox.Show("Game over!");
		}
		private void resetGame(){
			labelDash.Text="";
			target="";
			livesleft=5;
			noLives.Text=livesleft.ToString();
			textBoxGuess.Text="";
		}

	}
}
