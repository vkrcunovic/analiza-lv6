﻿/*
 * Created by SharpDevelop.
 * User: Krcko
 * Date: 30.1.2019.
 * Time: 20:49
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace SciCalc
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			double.TryParse(textBox1.Text, out prvi);
			double.TryParse(textBox2.Text, out drugi);
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		double prvi,drugi;
		bool unarno = true;
		void Button1Click(object sender, EventArgs e)
		{
			if(comboBoxOperacija.SelectedItem==null) MessageBox.Show("Odaberi operaciju!");
			else if (textBox1.Text == String.Empty) MessageBox.Show("Unesi prvi operand!", "Pogreška");
			else if (textBox2.Text == String.Empty && (comboBoxOperacija.SelectedItem.ToString()=="zbrajanje"||
			         comboBoxOperacija.SelectedItem.ToString()=="oduzimanje"||
			         comboBoxOperacija.SelectedItem.ToString()=="množenje"||
			         comboBoxOperacija.SelectedItem.ToString()=="dijeljenje")) {MessageBox.Show("Drugi operand!");
				textBox2.Focus();}
			else if (!double.TryParse(textBox1.Text, out prvi)) MessageBox.Show("1. operand nije broj!");
			else if (!double.TryParse(textBox2.Text, out drugi)&& !(textBox2.Text==String.Empty)) MessageBox.Show("2. operand nije broj!");
			else if(comboBoxOperacija.SelectedItem.ToString()=="zbrajanje") rezultat.Text = (prvi+drugi).ToString();
			else if(comboBoxOperacija.SelectedItem.ToString()=="oduzimanje") rezultat.Text = (prvi-drugi).ToString();
			else if(comboBoxOperacija.SelectedItem.ToString()=="množenje") rezultat.Text = (prvi*drugi).ToString();
			else if(comboBoxOperacija.SelectedItem.ToString()=="dijeljenje") rezultat.Text = (prvi/drugi).ToString();
			else if(comboBoxOperacija.SelectedItem.ToString()=="kvadriranje") {
				rezultat.Text = (Math.Pow(prvi,2).ToString());
				textBox2.Text="";
				textBox2.ReadOnly=true;}
			else if(comboBoxOperacija.SelectedItem.ToString()=="drugi korijen") {
				rezultat.Text = (Math.Sqrt(prvi).ToString());
				textBox2.Text="";
				textBox2.ReadOnly=true;
			}
			else if(comboBoxOperacija.SelectedItem.ToString()=="sinus") {
				rezultat.Text = (Math.Sin(prvi).ToString());
				textBox2.Text="";
				textBox2.ReadOnly=true;
			}
			else if(comboBoxOperacija.SelectedItem.ToString()=="cosinus") {
				rezultat.Text = (Math.Cos(prvi).ToString());
				textBox2.Text="";
				textBox2.ReadOnly=true;
			}
			else if(comboBoxOperacija.SelectedItem.ToString()=="log10") {
				rezultat.Text = (Math.Log10(prvi).ToString());
				textBox2.Text="";
				textBox2.ReadOnly=true;
			}
		}
		void TextBox3Click(object sender, EventArgs e)
		{
			rezultat.SelectAll();
			rezultat.Focus();
		}
		
	}
}
