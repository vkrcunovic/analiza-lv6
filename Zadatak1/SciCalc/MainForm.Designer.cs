﻿/*
 * Created by SharpDevelop.
 * User: Krcko
 * Date: 30.1.2019.
 * Time: 20:49
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace SciCalc
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox rezultat;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox comboBoxOperacija;
		private System.Windows.Forms.Button button1;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.rezultat = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.comboBoxOperacija = new System.Windows.Forms.ComboBox();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(69, 12);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(203, 20);
			this.textBox1.TabIndex = 0;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(69, 38);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(203, 20);
			this.textBox2.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(2, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(61, 16);
			this.label1.TabIndex = 2;
			this.label1.Text = "1. operand:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(2, 41);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(61, 16);
			this.label2.TabIndex = 3;
			this.label2.Text = "2. operand:";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(2, 69);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(61, 16);
			this.label4.TabIndex = 5;
			this.label4.Text = "Operacija:";
			// 
			// rezultat
			// 
			this.rezultat.Location = new System.Drawing.Point(69, 130);
			this.rezultat.Name = "rezultat";
			this.rezultat.ReadOnly = true;
			this.rezultat.Size = new System.Drawing.Size(203, 20);
			this.rezultat.TabIndex = 6;
			this.rezultat.Click += new System.EventHandler(this.TextBox3Click);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(2, 133);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(61, 16);
			this.label3.TabIndex = 7;
			this.label3.Text = "Rezultat:";
			// 
			// comboBoxOperacija
			// 
			this.comboBoxOperacija.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxOperacija.FormattingEnabled = true;
			this.comboBoxOperacija.Items.AddRange(new object[] {
			"zbrajanje",
			"oduzimanje",
			"množenje",
			"dijeljenje",
			"kvadriranje",
			"drugi korijen",
			"sinus",
			"cosinus",
			"log10"});
			this.comboBoxOperacija.Location = new System.Drawing.Point(69, 66);
			this.comboBoxOperacija.MaxDropDownItems = 9;
			this.comboBoxOperacija.Name = "comboBoxOperacija";
			this.comboBoxOperacija.Size = new System.Drawing.Size(203, 21);
			this.comboBoxOperacija.TabIndex = 8;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(69, 93);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(203, 31);
			this.button1.TabIndex = 9;
			this.button1.Text = "Izračunaj!";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 162);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.comboBoxOperacija);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.rezultat);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox1);
			this.Name = "MainForm";
			this.Text = "SciCalc";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
